import markovify
from time import sleep,time
import sys
import multiprocessing
import argparse
from os import path

__version__ = 2.0
__author__ = '(Lis)anne'

def prevLine():
	sys.stdout.write("\033[F")

def clrLine():
	sys.stdout.write("\033[K")

def clrPrevLine():
	prevLine()
	clrLine()




class fgclr:
	RED = '\033[31m'
	END = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'


def drawGraphs(data,colors=['\033[0;31m','\033[0;34m']):

	# god bless u kind stranger
	# https://alexwlchan.net/2018/05/ascii-bar-charts/
	max_value = max(count for _, count in data)
	increment = max_value / 25

	longest_label_length = max(len(label) for label, _ in data)+3
	color_track = 0
	for label, count in data:

		# The ASCII block elements come in chunks of 8, so we work out how
		# many fractions of 8 we need.
		# https://en.wikipedia.org/wikai/Block_Elements
		bar_chunks, remainder = divmod(int(count * 8 / increment), 8)

		# First draw the full width chunks
		bar = '█' * bar_chunks

		# Then add the fractional part.  The Unicode code points for
		# block elements are (8/8), (7/8), (6/8), ... , so we need to
		# work backwards.
		if remainder > 0:
			bar += chr(ord('█') + (8 - remainder))

		# If the bar is empty, add a left one-eighth block
		bar = bar or  '▏'

		print(f'{colors[color_track]}{label.rjust(longest_label_length)}\033[0m ▏{colors[color_track]}{count:8.4f} {bar}\033[0m')
		color_track +=1
		if color_track >= len(colors):
			color_track = 0


def getPercentBar(percentage):
	percentagestr = f'{percentage}%'
	bar = f'[{"#"*int(percentage/10)}{" "*int((100-percentage)/10)}]'
	output = f'{percentagestr} {" "*(4-len(percentagestr))}{bar}'
	return output

def worker(Q,archive,scale,state_size,updatefreq=10,dooutput=True,updatemoments=list(range(0,100,20))):
	me = multiprocessing.current_process()
	name = f'[{me.pid}] {me.name}'
	prfx = f'>      {name}:{" "*(19-len(name))}'
	newdata = []
	cycletimes = []
	updatecount = 0
	newtext = ''
	starttime = time()
	if dooutput: print(prfx,f'starting with scale {scale}, state size',state_size,'and an updatefreq of',updatefreq)
	model = markovify.NewlineText(archive,state_size=state_size)

	if dooutput: print(prfx,getPercentBar(0))
	for i in range(scale):
		for x in updatemoments:
			if int((i/scale)*100) == x:
				print(prfx,getPercentBar(x))
				break
		
		cycletime = time()
		if updatecount >= updatefreq:
			newtext = '\n'.join(newdata)
			model = markovify.NewlineText(f'{archive}\n{newtext}',state_size=state_size)
			updatecount = 0
		while (newline := model.make_sentence()) == None:
			pass
		newdata.append(newline)
		cycletimes.append(time()-cycletime)
		updatecount +=1

	print(prfx,getPercentBar(100))
	endtime = time()
	runtime = round(time()-starttime,1)
	newdata = list(dict.fromkeys(newdata))
	newtext = '\n'.join(newdata)
	output = {'name':name,'list':newdata,'string':newtext,'time':{'start':starttime,'end':endtime,'run':runtime},'cycles':scale,'averagecycletime':sum(cycletimes)/scale,'cycletimes':cycletimes}
	# if dooutput: print(prfx,'done, ran for',runtime,'seconds')
	Q.put(output)


def run(procAmount,archive,scale,state_size=3,output=None,updatefreq=10,maxTime=None,colors=['\033[1;32m','\033[1;36m']):
	# procamount is the number of processes created
	# archive is the input text (in newline format) for markovify
	# updatefreq is how often a process will update its own chain (higher = slower)
	# maxTime isnt even implemented
	# output is either an int or None, decides how many processes will print progress updates
	if type(procAmount) != int:
		procAmount = int(procAmount)
	if procAmount < 1:
		procAmount = 1

	if output == None or output >= procAmount:
		output = procAmount
	if type(output) != int:
		output = int(output)
	if procAmount > scale:
		procAmount = scale

	starttime = time()
	Q = multiprocessing.Queue()
	rawout = {}
	jobs = []
	scale = int(scale/procAmount)

	bld = '\033[1m'
	udl = '\033[2m'
	mjr = f'{colors[0]}==>   {bld}{udl}'
	mnr = f'{colors[1]}->   {udl}'
	end = '\033[0m'
	print(f'{mjr}first {output}/{procAmount} jobs will output',end)

	for i in range(procAmount):
		if i <= output:
			dooutput=True
		else:
			dooutput=False
		p = multiprocessing.Process(target=worker,args=(Q,archive,scale,state_size,updatefreq,dooutput))
		jobs.append(p)
		p.start()
	sleep(.02)
	print(f'{mjr}all jobs started',end)
	for index,p in enumerate(jobs):
		rawout[p.name] = Q.get()
		sleep(.02)
		if index <= output: print(f'{mnr}confirmed',p.name,'output',end)
	sleep(.02)
	print(f'{mjr}all job output confirmed',end)
	for index,p in enumerate(jobs):
		p.join()
		sleep(.02)
		if index <= output: print(f'{mnr}confirmed',p.name,'join',end)
	sleep(.02)
	print(f'{mjr}all job joins confirmed',end)
	summary = {}
	summary['procamount'] = procAmount
	summary['times'] = []
	summary['computetime'] = 0
	summary['list'] = []
	summary['string'] = ''
	summary['cycles'] = []
	summary['totalcycles'] = 0
	summary['averagecycletimes'] = []
	summary['cycletimes'] = []
	summary['statesize'] = state_size
	for item in rawout:
		summary['times'].append(rawout[item]['time']['run'])
		summary['computetime'] += rawout[item]['time']['run']
		summary['list'] += rawout[item]['list']
		summary['cycles'].append(rawout[item]['cycles'])
		summary['totalcycles'] += rawout[item]['cycles']
		summary['averagecycletimes'].append(rawout[item]['averagecycletime'])
		summary['cycletimes'] += rawout[item]['cycletimes']


	summary['list'] = list(dict.fromkeys(summary['list']))
	summary['string'] = '\n'.join(summary['list'])
	summary['runtime'] = time() - starttime
	return summary,rawout

def getArgs():
	parser = argparse.ArgumentParser(description='Generate buttonmashing using markov chains')
	parser.add_argument('-v','--version',		help='prints version',											action='store_true')
	parser.add_argument('-c','--combine',		help='combines output with input file',							action='store_true')
	parser.add_argument('-s','--statesize',		help='sets the markov chain statesize',							action='store', type=int, default=3)
	parser.add_argument('-l','--logcount',		help='sets the amount of processes that print progress updates',action='store', type=int, default=0)
	parser.add_argument('-p','--processes',		help='amount of processes created to split the work between',	action='store', type=int)
	parser.add_argument('input',				help='filename of input dataset file. see example.txt',			action='store',	type=str)
	parser.add_argument('lines',				help='amount of new lines of buttonmash',						action='store',	type=int)
	parser.add_argument('output',				help='name of output file where the mashes will be stored',		action='store',	type=str)

	return parser.parse_args()


if __name__ == '__main__':
	procLimit = 200

	# print('a good file should have a single buttonmash per line (see example.txt)')
	if len(sys.argv) > 1:
		if sys.argv[1] == '--version' or sys.argv[1] == '-v':
			print(f'MarkovMasher v{__version__} by {__author__}')
			exit()

	args = getArgs()
	if path.isfile(args.output):
		while (x := input(f'file "{args.output}" exists, continue?\n[y/n] ').lower()) != 'n' and x != 'y':
			pass
		if x == 'y':
			pass
		elif x == 'n':
			exit()
	if not path.isfile(args.input):
		print(f'file "{args.input}" doesnt exist')
		exit()

	with open(args.input,'r') as f:
		archive = f.read()
		if len(archive) < 10:
			print('archive too small	')
			exit()
		archive = archive.replace(' ','')
		archive = archive.replace('',' ')

	if args.processes:
		procAmount = args.processes
	else:
		procAmount = args.lines/100
		if procAmount > procLimit:
			procAmount = procLimit

	summary,raw = run(procAmount, archive, args.lines, state_size=args.statesize, output=args.logcount)
	print()
	# small summary of how things went
	print('--------------summary--------------')
	if len(summary['list']) != summary['totalcycles']:
		print('!! warning: linecount/cycle mismatch')
		print(f'!! difference of {abs(len(summary["list"])-summary["totalcycles"])}')
		print('-----------------------------------')
	red = '\033[31m'
	blu = '\033[34m'
	stp = '\033[0m'
	rgrn = f''#{stp}'#\033[42'
	bgrn = f''#{stp}'#\033[42'
	print(f'{red}process amount:        {rgrn}{summary["procamount"]}',stp)
	print(f'{blu}state size:            {bgrn}{summary["statesize"]}',stp)
	print(f'{red}run time:              {rgrn}{round(summary["runtime"]/60,2)}m / {round(summary["runtime"],2)}s',stp)
	print(f'{blu}compute time:          {bgrn}{round(summary["computetime"]/60,2)}m / {round(summary["computetime"],2)}s',stp)
	print(f'{red}average time:          {rgrn}{round(sum(summary["times"])/len(summary["times"]),3)} seconds',stp)
	print(f'{blu}new lines:             {bgrn}{len(summary["list"])}',stp)
	print(f'{red}total cycles:          {rgrn}{summary["totalcycles"]}',stp)
	print(f'{blu}average cycles:        {bgrn}{int(sum(summary["cycles"])/len(summary["cycles"]))}',stp)
	print(f'{red}average cycle time:    {rgrn}{round(sum(summary["averagecycletimes"])/len(summary["averagecycletimes"]),3)} seconds',stp)
	print(f'{blu}longest cycle time:    {bgrn}{round(max(summary["cycletimes"]),3)} seconds',stp)
	print(f'{red}shortest cycle time:   {rgrn}{round(min(summary["cycletimes"]),3)} seconds',stp)


	# prepares the output text, adds input if --combine is set
	if args.combine:
		outputText = f'{archive}\n{summary["string"]}'.replace(" ","")
	else:
		outputText = summary['string'].replace(" ","")

	# saves the thing
	with open(args.output,'w') as f:
		f.write(outputText)


	# stats about individual processes
	input('press enter for more details')
	clrPrevLine()
	print()
	print('--------------details--------------')
	print('         -total cycles-')
	drawGraphs([(item,raw[item]['cycles']) for item in raw])

	print()
	print('          -runtime (s)-')
	drawGraphs([(item,raw[item]['time']['run']) for item in raw])

	print()
	print('     -average cycle time (s)-')
	drawGraphs([(item,raw[item]['averagecycletime']) for item in raw])
	
	print()
	print('     -longest cycle time (s)-')
	drawGraphs([(item,max(raw[item]['cycletimes'])) for item in raw])

	print()
	print('     -shortest cycle time (s)-')
	drawGraphs([(item,min(raw[item]['cycletimes'])) for item in raw])

	print()
	print('    -generated character count-')
	drawGraphs([(item,len(raw[item]['string'])) for item in raw])