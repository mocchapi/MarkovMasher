# MarkovMasher
python3 script that generates buttonmashes using markov chains

requires the `markovify` package from pypi
run `pip -r requirements.txt` to install

## Usage:

usage: [-h] [-v] [-c] [-s STATESIZE] [-l LOGCOUNT] [-p PROCESSES] input lines output

positional arguments:
  input                 filename of input dataset file. see example.txt
  lines                 amount of new lines of buttonmash
  output                name of output file where the mashes will be stored

optional arguments:
  -h, --help                               show help message and exit
  -v, --version                            show version and exit
  -c, --combine                            combines output with input file
  -s STATESIZE, --statesize statesize      sets the markov chain statesize
  -l LOGCOUNT, --logcount LOGCOUNT         sets the amount of processes that print progress updates
  -p PROCESSES, --processes PROCESSES      amount of processes created to split the work between